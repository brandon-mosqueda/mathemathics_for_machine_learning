Covid19Data <- read.csv(file="200506COVID19MEXICO_Reduced.csv", header=TRUE, sep=",")
head(Covid19Data)
tail(Covid19Data)
Covid19Data=Covid19Data[,-c(1,4)]
Covid19Data=Covid19Data[Covid19Data$RESULTADO==1,]
dim(Covid19Data)
Covid19Data=Covid19Data[Covid19Data$DIABETES<=2,]
dim(Covid19Data)
Covid19Data=Covid19Data[Covid19Data$EPOC<=2,]
dim(Covid19Data)
Covid19Data=Covid19Data[Covid19Data$ASMA<=2,]
dim(Covid19Data)
Covid19Data=Covid19Data[Covid19Data$INMUSUPR<=2,]
dim(Covid19Data)
Covid19Data=Covid19Data[Covid19Data$HIPERTENSION<=2,]
dim(Covid19Data)
Covid19Data=Covid19Data[Covid19Data$CARDIOVASCULAR<=2,]
dim(Covid19Data)
Covid19Data=Covid19Data[Covid19Data$OBESIDAD<=2,]
dim(Covid19Data)
Covid19Data=Covid19Data[Covid19Data$RENAL_CRONICA<=2,]
dim(Covid19Data)

Covid19Data=Covid19Data[Covid19Data$TIPO_PACIENTE<=2,]
dim(Covid19Data)
table(Covid19Data$TIPO_PACIENTE)
pos_int_2=which(Covid19Data$TIPO_PACIENTE==2)
pos_int_1=which(Covid19Data$TIPO_PACIENTE==1)
length(pos_int_2)
set.seed(2)
Sample_pos_int_2=pos_int_2[sample(11086,2000)]
Sample_pos_int_1=pos_int_1[sample(16211,2000)]
pos_joint=c(Sample_pos_int_1,Sample_pos_int_2)
length(pos_joint)
Covid19Data=Covid19Data[pos_joint,]
dim(Covid19Data)
table(Covid19Data$TIPO_PACIENTE)

dim(Covid19Data)
colnames(Covid19Data)
Covid19Data$SEXO=as.factor(Covid19Data$SEXO)
Covid19Data$TIPO_PACIENTE=as.factor(Covid19Data$TIPO_PACIENTE)
Covid19Data$INTUBADO=as.factor(Covid19Data$INTUBADO)
Covid19Data$DIABETES=as.factor(Covid19Data$DIABETES)
Covid19Data$EPOC=as.factor(Covid19Data$EPOC)
Covid19Data$ASMA=as.factor(Covid19Data$ASMA)
Covid19Data$INMUSUPR=as.factor(Covid19Data$INMUSUPR)
Covid19Data$HIPERTENSION=as.factor(Covid19Data$HIPERTENSION)
Covid19Data$CARDIOVASCULAR=as.factor(Covid19Data$CARDIOVASCULAR)
Covid19Data$OBESIDAD=as.factor(Covid19Data$OBESIDAD)
Covid19Data$RENAL_CRONICA=as.factor(Covid19Data$RENAL_CRONICA)
Covid19Data$TABAQUISMO=as.factor(Covid19Data$TABAQUISMO)
Covid19Data$UCI=as.factor(Covid19Data$UCI)

Covid19Data$TIPO_PACIENTE=ifelse(Covid19Data$TIPO_PACIENTE==2,0,1)
train=sample(1:length(Covid19Data$TIPO_PACIENTE),0.9*length(Covid19Data$TIPO_PACIENTE))
length(train)


#using R package 
M1 = glm(TIPO_PACIENTE~SEXO+EDAD+DIABETES+EPOC+ASMA+INMUSUPR+HIPERTENSION+CARDIOVASCULAR+OBESIDAD+RENAL_CRONICA, subset=train,family = "binomial",data=Covid19Data)
summary(M1)
M1$coefficients

Tst=Covid19Data[-train,]

#glm.probs2=predict(glm.fit2,Tst, type="response")
glm.probs2=predict(M1,Tst, type="response")
length(glm.probs2)
glm.probs2

pred_values2=ifelse(glm.probs2>0.5,2,1)
pred_values2
Observed2=Tst$TIPO_PACIENTE
head(cbind(Observed2,pred_values2))
tail(cbind(Observed2,pred_values2))

Conf_Matrix2=table(pred_values2,Observed2)
Conf_Matrix2

####Percentage of cases correctly classified####
PCCC2=sum(diag(Conf_Matrix2)/length(Observed2))
PCCC2

# k-Fold Cross-Validation with only horsepower as predictor
cost <- function(y, pi = 0) mean(abs(y-pi) < 0.5)
library(boot)
set.seed(17)
cv.error.10=rep(0,10)
for (i in 1:10){
  glm.fit=glm(TIPO_PACIENTE~SEXO+EDAD+DIABETES+EPOC+ASMA+INMUSUPR+HIPERTENSION+CARDIOVASCULAR+OBESIDAD+RENAL_CRONICA,data=Covid19Data, family=binomial)
  cv.error.10[i]=cv.glm(Covid19Data,glm.fit,cost,K=10)$delta[1]
}
cv.error.10
mean(cv.error.10)
sd(cv.error.10)


