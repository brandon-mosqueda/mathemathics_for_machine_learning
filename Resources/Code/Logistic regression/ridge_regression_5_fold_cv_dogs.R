# Delete all objects in our enviroment
rm(list=ls())

library(ISLR)
library(glmnet)
library(imager)

# Import dataset
load("2_is_dog_dataset.RData")

# For reproducible results
set.seed(1)

# 4000 images of 32 x 32 (1024 pixeles)
# 2000 dogs' images (Y = 1), and 2000 of other categories (Y = 0)
dim(X)
head(Y)

# Given a vector of numeric values print the corresponding image
print_image <- function(vector_image, x=32, y=32, z=1) {
    image <- as.cimg(vector_image, x=x, y=y, z=z)
    plot(image)
}

dogs_indexes <- which(Y == 1)
no_dogs_indexes <- which(Y != 1)

# Dogs images
image <- unlist(X[dogs_indexes[1], ], use.names=FALSE) # Convert to vector
print_image(image)
image <- unlist(X[dogs_indexes[2], ], use.names=FALSE)
print_image(image)
image <- unlist(X[dogs_indexes[3], ], use.names=FALSE)
print_image(image)

# Other categories images
image <- unlist(X[no_dogs_indexes[1], ], use.names=FALSE)
print_image(image)
image <- unlist(X[no_dogs_indexes[2], ], use.names=FALSE)
print_image(image)
image <- unlist(X[no_dogs_indexes[3], ], use.names=FALSE)
print_image(image)

########Selecting only a fraction of the data
Y=Y[c(dogs_indexes[1:300],no_dogs_indexes[1:300])] 
X=X[c(dogs_indexes[1:300],no_dogs_indexes[1:300]),]

# Proportion of cases -correctly classified
pccc <- function(observed, predicted) mean(observed == predicted)

CV_FOLDS_NUM <- 5
N <- nrow(X)
Grpv_k = findInterval(cut(sample(1:N, N), breaks=CV_FOLDS_NUM), 1:N)
results <- data.frame()

for (k in 1:CV_FOLDS_NUM) {
    cat("k = ", k, "\n")
    set.seed(k)

    training_indexes <- which(Grpv_k != k)

    Y_training <- Y[training_indexes]
    X_training <- X[training_indexes, ]

    Y_testing <- Y[-training_indexes]
    X_testing <- X[-training_indexes, ]

    # RR == Ridge regression
    RR_model <- cv.glmnet(X_training, Y_training, family='binomial',
                          alpha=0, type.measure='class', grouped=FALSE,
                          nfolds=3)
    predictions_RR <- as.numeric(predict(RR_model, newx=as.matrix(X_testing),
                                         s='lambda.min', type='class'))

    RR_PCCC <- pccc(Y_testing, predictions_RR)
    cat("RR_PCCC = ", RR_PCCC, "\n")

    # LR == Lasso regression
    LR_model <- cv.glmnet(X_training, Y_training, family='binomial',
                          alpha=1, type.measure='class', grouped=TRUE,
                          nfolds=3)
    predictions_LR <- as.numeric(predict(LR_model, newx=as.matrix(X_testing),
                                         s='lambda.min', type='class'))

    LR_PCCC <- pccc(Y_testing, predictions_LR)
    cat("LR_PCCC = ", LR_PCCC, "\n")

    results <- rbind(results, data.frame(RR_PCCC, LR_PCCC))
    cat("**************\n\n")
}

rownames(results) <- paste0("k = ", 1:CV_FOLDS_NUM)
results
####Mean across folds
apply(results,2,mean)
####sd across folds
apply(results,2,mean)