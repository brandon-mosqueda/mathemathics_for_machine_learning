rm(list=ls())
library("ISLR")
library(glmnet)
dim(Auto)
head(Auto)

#setwd("~/Facultad de Telem�tica/Sexto Semestre/Maestr�a/Tareas/Tarea 4/C�digo")
#source("Utils.R")

#### Primer ejercicio ####
## Guardamos todos los datos.
Datos <- data.frame(y=Auto$mpg, x1=Auto$horsepower, x2=Auto$weight)
head(Datos)
#############10 random partitions cross validation###############
Tab <- data.frame()
nCV=5
for(k in 1:nCV) {
#k=1
set.seed(k)
indices_training <-sample(1:dim(Datos)[1], 0.8*dim(Datos)[1])
Data_trn <- Datos[indices_training,]
Data_tst <- Datos[-indices_training,]

y_tr <- Data_trn$y
X_tr <-model.matrix(~0+x1+x2,data=Data_trn)
y_tst <- Data_tst$y
X_tst <-Data_tst[,-1]

# RR == Ridge regression
A_RR <- cv.glmnet(X_tr, y_tr, family='gaussian',
                  alpha=0.0, type.measure='mse', grouped=F)
yp_RR <- as.numeric(predict(A_RR, newx=as.matrix(X_tst), s='lambda.min'))
plot(y_tst,yp_RR)
# LR == Lasso regression
A_LR <- cv.glmnet(X_tr, y_tr, family='gaussian',
                  alpha=1, type.measure='mse', grouped=T)
yp_LR <- as.numeric(predict(A_LR, newx=as.matrix(X_tst), s='lambda.min'))
plot(y_tst,yp_LR)
# OLS == Ordinal least square
A_OLS <- glmnet(X_tr, y_tr, family='gaussian', alpha=0, lambda=0)
yp_OLS <- as.numeric(predict(A_OLS, newx=as.matrix(X_tst)))
plot(y_tst,yp_OLS)

Tab <- rbind(Tab, data.frame(MSE_RR=mean((y_tst-yp_RR)^2),
                             MSE_LR=mean((y_tst - yp_LR)^2),
                             MSE_OLS= mean((y_tst - yp_OLS)^2)))
}
Tab
Mean_v <- colMeans(Tab)
Mean_v 

apply(Tab[,-1], 2, sd)

#########10 Fold Cross validation######################
Tab2 <- data.frame()
nCV=5
n_data=dim(Datos)[1]
Grpv_k = findInterval(cut(sample(1:n_data,n_data),breaks=nCV),1:n_data)
Grpv_k 
for(k in 1:nCV) {
 # k=1
  set.seed(k)
  n_data=dim(Datos)[1]
  n_data
  indices_training<-which(Grpv_k!=k)
  indices_training
  
  Data_trn <- Datos[indices_training,]
  
  Data_tst <- Datos[-indices_training,]
  
  y_tr <- Data_trn$y
y_tr <- Data_trn$y
X_tr <-model.matrix(~0+x1+x2,data=Data_trn)

y_tst <- Data_tst$y
X_tst <-Data_tst[,-1]

# RR == Ridge regression
A_RR <- cv.glmnet(X_tr, y_tr, family='gaussian',
                  alpha=0.0, type.measure='mse', grouped=F)
yp_RR <- as.numeric(predict(A_RR, newx=as.matrix(X_tst), s='lambda.min'))
plot(y_tst,yp_RR)
# LR == Lasso regression
A_LR <- cv.glmnet(X_tr, y_tr, family='gaussian',
                  alpha=1, type.measure='mse', grouped=T)
yp_LR <- as.numeric(predict(A_LR, newx=as.matrix(X_tst), s='lambda.min'))
plot(y_tst,yp_LR)
# OLS == Ordinal least square
A_OLS <- glmnet(X_tr, y_tr, family='gaussian', alpha=0, lambda=0)
yp_OLS <- as.numeric(predict(A_OLS, newx=as.matrix(X_tst)))
plot(y_tst,yp_OLS)

Tab2 <- rbind(Tab2, data.frame(MSE_RR=mean((y_tst-yp_RR)^2),
                             MSE_LR=mean((y_tst - yp_LR)^2),
                             MSE_OLS= mean((y_tst - yp_OLS)^2)))
}
Tab2
Tab

