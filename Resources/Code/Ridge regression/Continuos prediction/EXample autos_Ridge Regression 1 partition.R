rm(list=ls())
library("ISLR")
library(glmnet)
set.seed(1)
dim(Auto)
head(Auto)

#setwd("~/Facultad de Telem�tica/Sexto Semestre/Maestr�a/Tareas/Tarea 4/C�digo")
#source("Utils.R")

#### Primer ejercicio ####
## Guardamos todos los datos.
Datos <- data.frame(y=Auto$mpg, x1=Auto$horsepower, x2=Auto$weight)
head(Datos)

### Establecemos los �ndices de los datos de entrenamiento.
indices_training <- 1:313

## Establecemos los �ndices de los datos de testeo.
indices_testing <- 314:392

## Datos de entrenamiento.
Data_trn <- Datos[indices_training,]
head(Data_trn)

## Datos de testeo.
Data_tst <- Datos[indices_testing,]

n_training <- length(predicciones)

Tab <- data.frame()
y_tr <- Data_trn$y
X_tr <-model.matrix(~0+x1+x2,data=Data_trn)
X_tr

y_tst <- Data_tst$y
X_tst <-Data_tst[,-1]
str(X_tst)
# RR == Ridge regression
A_RR <- cv.glmnet(X_tr, y_tr, family='gaussian',
                  alpha=0.0, type.measure='mse', grouped=F)
yp_RR <- as.numeric(predict(A_RR, newx=as.matrix(X_tst), s='lambda.min'))
plot(y_tst,yp_RR)
# LR == Lasso regression
A_LR <- cv.glmnet(X_tr, y_tr, family='gaussian',
                  alpha=1, type.measure='mse', grouped=T)
yp_LR <- as.numeric(predict(A_LR, newx=as.matrix(X_tst), s='lambda.min'))
plot(y_tst,yp_LR)
# OLS == Ordinal least square
A_OLS <- glmnet(X_tr, y_tr, family='gaussian', alpha=0, lambda=0)
yp_OLS <- as.numeric(predict(A_OLS, newx=as.matrix(X_tst)))
plot(y_tst,yp_OLS)


Tab <- rbind(Tab, data.frame(MSE_RR=mean((y_tst-yp_RR)^2),
                             MSE_LR=mean((y_tst - yp_LR)^2),
                             MSE_OLS= mean((y_tst - yp_OLS)^2)))

Tab

#########1 hold out random partition######################
### Establecemos los �ndices de los datos de entrenamiento.
indices_training <-sample(dim(Datos)[1], 0.8*dim(Datos)[1])
indices_training


## Datos de entrenamiento.
Data_trn <- Datos[indices_training,]
head(Data_trn)

## Datos de testeo.
Data_tst <- Datos[-indices_training,]

n_training <- dim(Data_trn)[1]
Tab2 <- data.frame()
y_tr <- Data_trn$y
X_tr <-model.matrix(~0+x1+x2,data=Data_trn)
X_tr

y_tst <- Data_tst$y
X_tst <-Data_tst[,-1]
str(X_tst)
# RR == Ridge regression
A_RR <- cv.glmnet(X_tr, y_tr, family='gaussian',
                  alpha=0.0, type.measure='mse', grouped=F)
yp_RR <- as.numeric(predict(A_RR, newx=as.matrix(X_tst), s='lambda.min'))
plot(y_tst,yp_RR)
# LR == Lasso regression
A_LR <- cv.glmnet(X_tr, y_tr, family='gaussian',
                  alpha=1, type.measure='mse', grouped=T)
yp_LR <- as.numeric(predict(A_LR, newx=as.matrix(X_tst), s='lambda.min'))
plot(y_tst,yp_LR)
# OLS == Ordinal least square
A_OLS <- glmnet(X_tr, y_tr, family='gaussian', alpha=0, lambda=0)
yp_OLS <- as.numeric(predict(A_OLS, newx=as.matrix(X_tst)))
plot(y_tst,yp_OLS)


Tab2 <- rbind(Tab2, data.frame(MSE_RR=mean((y_tst-yp_RR)^2),
                             MSE_LR=mean((y_tst - yp_LR)^2),
                             MSE_OLS= mean((y_tst - yp_OLS)^2)))

Tab2
Tab

