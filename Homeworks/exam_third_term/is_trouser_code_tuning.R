# Declaring the flags for hyperparameters
# FLAGS <- flags(flag_numeric("dropout1", 0.05),
#                flag_integer("units1", 50),
#                flag_string("activation1", "relu"),
#                flag_numeric("dropout2", 0.05),
#                flag_integer("units2", 50),
#                flag_string("activation2", "relu"),
#                flag_integer("Epoch1", 1000),
#                flag_numeric("val_split",0.2))
FLAGS <- list(
  dropout1= 0.05,
  units1= 50,
  activation1= "relu",
  dropout2= 0.05,
  units2= 50,
  activation2= "relu",
  Epoch1= 1000,
  val_split=0.2)

print(FLAGS)

# Defining the DNN  model
model <- keras_model_sequential()
model %>% layer_dense(units=FLAGS$units1,
                      activation=FLAGS$activation1,
                      input_shape=ncol(X_training)) %>%
          layer_dropout(rate=FLAGS$dropout1) %>%
          layer_dense(units=FLAGS$units2,
                      activation=FLAGS$activation2) %>%
          layer_dropout(rate=FLAGS$dropout1) %>%
          layer_dense(units=1, activation="sigmoid")

# Compiling the DNN model
model %>% compile(loss="binary_crossentropy",
                  optimizer="rmsprop",
                  metrics=c("accuracy"))

print_dot_callback <- callback_lambda(
  on_epoch_end = function(epoch, logs) {
    if (epoch %% 20 == 0) cat("\n")
    cat(".")})

early_stop <- callback_early_stopping(monitor="val_loss",
                                      mode='min',
                                      patience=50)

# Fitting the DNN model
model %>% fit(X_training, Y_training,
              epochs=FLAGS$Epoch1,
              batch_size=36,
              shuffled=FALSE, validation_split=FLAGS$val_split,
              verbose=0,
              callbacks=list(early_stop, print_dot_callback))