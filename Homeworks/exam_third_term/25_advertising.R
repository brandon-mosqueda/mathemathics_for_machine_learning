# rm(list=ls())

library(keras)
library(caret)

set.seed(448)
use_session_with_seed(448)

Data <- read.csv("Advertising.csv")
# Eliminar la primera columna
Data <- Data[, -1]

predictors <- c("TV", "radio", "newspaper")
response <- "sales"

# Mean square error
mse <- function(actual, predicted) mean((actual - predicted)^2)

N <- nrow(Data)
n_epochs <- 500
bootstrap_num <- 5

results <- c()
# k <- 1
for (k in 1:bootstrap_num) {
    # cat("k =", k, "\n")
    training_indices <- sample(N, N, replace=TRUE)
    testing_indices <- (1:N)[-unique(training_indices)]

    X_training <- as.matrix(Data[training_indices, predictors])
    X_testing <- as.matrix(Data[testing_indices, predictors])

    Y_training <- Data[training_indices, response]
    Y_testing <- Data[testing_indices, response]

    # Model's specification
    network <- keras_model_sequential()

    network %>% layer_dense(units=75, activation="relu",
                            input_shape=length(predictors)) %>%
                layer_dense(units=75, activation="relu") %>%
                layer_dense(units=1)

    network %>% compile(loss="mae",
                        optimizer="rmsprop",
                        metrics=c("mae"))

    model_fit <- network %>% fit(
      X_training, Y_training,
      shuffle=FALSE,
      epochs=n_epochs,
      batch_size=36,
      validation_split=0.3,
      verbose=0)

    predictions <- network %>% predict(X_testing)

    results <- c(results, mse(Y_testing, predictions))
}

results

# Capacidad predictiva final
mean(results)