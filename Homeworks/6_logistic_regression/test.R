rm(list=ls())
library(ISLR)
library(vcd) # For kappa coefficient function
library(glmnet)

# Para resultados reproducibles
set.seed(5639)

Iris_data <- iris
N <- nrow(Iris_data)

Iris_data$y <- ifelse(Iris_data$Species == "setosa", 1, 0)
Iris_data <- Iris_data[sample(N), ]

head(Iris_data)

Iris_data$y <- as.factor(Iris_data$y)
# Delete Species column
Iris_data$Species <- NULL

# Proporción de casos correctament clasificados
pccc <- function(actual, predicted) sum(actual == predicted) / length(actual)

Y_INDEX <- 5
training_indices <- sample(N, 0.5 * N)

X_training <- as.matrix(Iris_data[training_indices, -Y_INDEX])
X_testing <- as.matrix(Iris_data[-training_indices, -Y_INDEX])
y_training <- Iris_data[training_indices, Y_INDEX]
y_testing <- Iris_data[-training_indices, Y_INDEX]
N_testing <- length(y_testing)

Data_training <- Iris_data[training_indices, ]
Data_testing <- Iris_data[-training_indices, ]

# logistic_model <- glm(y ~ ., data=Data_training,
logistic_model <- glm(y ~ Sepal.Length + Sepal.Width +
                          Petal.Length + Petal.Width,
                      data=Data_training,
                      family="binomial")
summary(logistic_model)
logistic_predictions <- predict(logistic_model, Data_testing, type="response")
head(logistic_predictions)
tail(logistic_predictions)
ifelse(logistic_predictions > 0.5, 1, 0)

pccc(y_testing, logistic_predictions)

png(file="test.png", width=1000, height=850, res=110)
par(mar=c(5, 5, 5, 8.1), xpd=TRUE)
plot(1:N_testing, logistic_predictions,
     ylim=c(0, 1),
     pch=21, col="blue",
     main="Observed and predicted comparasion",
     xlab="Observations", ylab="Responses", lwd=1)
points(1:N_testing, as.numeric(y_testing) - 1, col="red", pch=21, lwd=1)
legend("topright", legend=c("Predictions", "Observed"),
       pch=c(21, 21), col=c("blue", "red"), inset=c(-0.7, 0))
dev.off()
par()
plot(c(1, N_testing), c(0, 1),
     type="n", xlab="Individuos", ylab="Ventas (sales)")
lines(1:N_testing, logistic_predictions, type="l", lwd=1.5, col="blue", lty=1)
points(1:N_testing, as.numeric(y_testing) - 1, col="red", pch=21)
# lines(1:N_testing, as.numeric(y_testing) - 1, type="l", lwd=1.5, lty=1, col="red")
legend("bottomright", legend=c("Predicciones", "Observados"),
       lty=1, lwd=1.5, col=c("blue", "red"), bg="white", inset=c(-0.2, 0))

logistic_ridge_model <- cv.glmnet(X_training, y_training,
                                  family="binomial", alpha=0,
                                  type.measure="class")
summary(logistic_ridge_model)
logistic_ridge_predictions <- predict(logistic_ridge_model,
                                      newx=X_testing,
                                      s="lambda.min",
                                      type="class")
head(logistic_ridge_predictions)
tail(logistic_ridge_predictions)